﻿using System;
using Scribe.Core.ConnectorApi.Actions;

namespace Scribe.Connector.FileStream.DataAccess
{
    internal interface IDataAccess : IDisposable 
    {
        //TODO Fill this is and implement
        OperationResult ExecuteInsert(string filePath);
        OperationResult ExecuteInsert(string filePath, byte[] fileBlob);
    }
}
