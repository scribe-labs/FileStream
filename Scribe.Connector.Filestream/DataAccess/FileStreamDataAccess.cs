﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.Logger;

namespace Scribe.Connector.FileStream.DataAccess
{
    public class FileStreamDataAccess : IDataAccess
    {
        private byte[] fileData;
        #region ctor
        public FileStreamDataAccess()
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public OperationResult ExecuteInsert(string filePath)
        {
            var result = new OperationResult();

            try
            {
                OpenFile(filePath);

                var fileName = Path.GetFileName(filePath);
                result = PopulateOperationResult(fileName, fileData);
            }
            catch (FileNotFoundException fnf)
            {
                var errorInfo = new ErrorResult
                {
                    Description = String.Format("File Was Not Found"),
                    Detail = fnf.Message
                };
                result.ErrorInfo = new[] { errorInfo };
                //result.ErrorInfo[0] = new ErrorResult();
                //result.ErrorInfo[0].Description = fnf.Message.ToString();
                //result.ErrorInfo[0].Detail = fnf.InnerException.ToString();
                Logger.Write(Logger.Severity.Error, "FileStreamDataAccess", fnf.Message);
            }
            catch (FileLoadException fnf)
            {
                var errorInfo = new ErrorResult
                {
                    Description = String.Format("File Already Exists"),
                    Detail = fnf.Message
                };
                result.ErrorInfo = new[] { errorInfo };
                Logger.Write(Logger.Severity.Error, "FileStreamDataAccess", fnf.Message);
            }
            catch (Exception ex)
            {
                var errorInfo = new ErrorResult
                {
                    Description = String.Format("Unknown Error"),
                    Detail = ex.Message
                };
                result.ErrorInfo = new[] { errorInfo };
                Logger.Write(Logger.Severity.Error, "FileStreamDataAccess", ex.Message);
            }

            return result;            
        }
        public OperationResult ExecuteInsert(string filePath, byte[] fileBlob)
        {
            var result = new OperationResult();

            try
            {
                var fileName = Path.GetFileName(filePath);
                fileData = fileBlob;
                WriteFile(filePath);

                result = PopulateOperationResult(fileName, fileData);
            }
            catch (FileLoadException fnf)
            {
                var errorInfo = new ErrorResult
                {
                    Description = String.Format("File Already Exists"),
                    Detail = fnf.Message
                };
                result.ErrorInfo = new[] { errorInfo };
                Logger.Write(Logger.Severity.Error, "FileStreamDataAccess", fnf.Message);
            }
            catch (Exception ex)
            {
                var errorInfo = new ErrorResult
                {
                    Description = String.Format("Unknown Error"),
                    Detail = ex.Message
                };
                result.ErrorInfo = new[] { errorInfo };
                Logger.Write(Logger.Severity.Error, "FileStreamDataAccess", ex.Message);
            }

            return result;
        }
        private void OpenFile(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                var rawData = File.ReadAllBytes(filePath);
                fileData = rawData;
            }
            else
            {
                throw new FileNotFoundException();
            }            
        }


        private void WriteFile(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                throw new FileLoadException("File Already Exists");
            }
            else
            {
                // Write out the decoded data.
                System.IO.FileStream outFile;
                try
                {
                    outFile = new System.IO.FileStream(filePath,
                                               System.IO.FileMode.Create,
                                               System.IO.FileAccess.Write);
                    outFile.Write(fileData, 0, fileData.Length);
                    outFile.Close();
                }
                catch (System.Exception exp)
                {
                    // Error creating stream or writing to it.
                    System.Console.WriteLine("{0}", exp.Message);
                }
            }
        }

        private OperationResult PopulateOperationResult(string fileName, byte[] fileData)
        {
            var operationResult = new OperationResult();
            
            var dataEntity = new DataEntity();
            dataEntity.Properties.Add(Metadata.MetadataProvider.FILENAME, fileName);
            dataEntity.Properties.Add(Metadata.MetadataProvider.FILEDATA, this.fileData);
            dataEntity.ObjectDefinitionFullName = Metadata.MetadataProvider.FILE;
            operationResult.Output = new[] { dataEntity };
            operationResult.Success = new[] { true };

            return operationResult;
        }

        public void Dispose()
        {
            this.fileData = null;
            GC.Collect();
        }
    }
}
