﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scribe.Connector.FileStream.DataAccess
{
    internal class DataAccessProvider : IDataAccessProvider
    {
        public IDataAccess GetDataAccess(string path)
        {
            return new FileStreamDataAccess();
        }
    }
}
