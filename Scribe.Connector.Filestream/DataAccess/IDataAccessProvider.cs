﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scribe.Connector.FileStream.DataAccess
{
    internal interface IDataAccessProvider
    {

        IDataAccess GetDataAccess(string path);

    }
}
