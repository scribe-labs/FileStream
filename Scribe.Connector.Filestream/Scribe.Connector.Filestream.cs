﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scribe.Connector.FileStream.DataAccess;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.ConnectionUI;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.Query;
using Scribe.Connector.FileStream.Operations;


namespace Scribe.Connector.FileStream
{
    
    [ScribeConnector(ConnectorTypeIdAsString, ConnectorTypeName, ConnectorTypeDescription, typeof(FileStreamConnector),
        SettingsUITypeName, SettingsUIVersion, ConnectionUITypeName, ConnectionUIVersion, XapFileName,
                new string[] {"Scribe.IS2.Target"}, SupportsCloud, ConnectorVersion)]
    public class FileStreamConnector : IConnector
    {

        #region constants

        private const string RootFolderPropertyName = "RootFolderPath";

        private bool isConnected;

        #endregion

        #region Members used by core for reflection
        internal const string ConnectorVersion = "1.1.0.2";

        internal const string SettingsUITypeName = "";

        internal const string SettingsUIVersion = "1.0";

        internal const string ConnectionUITypeName = "ScribeOnline.GenericConnectionUI";

        internal const string ConnectionUIVersion = "1.0";

        internal const string XapFileName = "ScribeOnline";

        internal const string ConnectorTypeIdAsString = "B7E773FB-8927-4F18-9331-F6F812AD3382";

        internal const string ConnectorTypeName = "Scribe Labs – FileStream";

        internal const string ConnectorTypeDescription = "A Scribe Labs Connector for Reading and Writing Files. Please see http://www.scribesoft.com/scribelabs for important details.";

        internal const string MetadataPrefix = "Scribe Labs – FileStream";

        internal const bool SupportsCloud = false;

        internal const string cryptoKey = "87E6F69F-AEDC-4C23-8B40-FA468439AF83";
        #endregion

        public bool IsConnected
        {
            get { return this.isConnected; }
        }

        private IDataAccessProvider dataAccessProvider;

        private IMetadataProvider metadataProvider;

        public FileStreamConnector()
        {
            InitializeConnector();
        }
       
        public void Connect(IDictionary<string, string> properties)
        {
            if (!isConnected)
            {
                if (!ValidateConnectionParameters(properties))
                {
                    var path = properties.FirstOrDefault(kvp => kvp.Key == RootFolderPropertyName).Value;

                    throw new ArgumentNullException(RootFolderPropertyName, string.Format("Could not find file path: {0}", path));
                }
                else
                {
                    isConnected = ValidateConnectionParameters(properties);
                }
                  this.InitializeConnector();
            }
            //TODO throw if folder doesn't exist.
          
        }

        public Guid ConnectorTypeId
        {
            get { return this.ConnectorTypeId; }
        }

        public void Disconnect()
        {
            this.isConnected = false;
        }

        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();
        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            OperationResult operationResult = null;
            var operationProvider = new OperationProvider();
        
            using (IOperation operation = operationProvider.GetOperation(input, this.dataAccessProvider))
            {
                try
                {
                    operationResult = operation.Execute(input);
                }
                catch (Exception ex)
                {
                    //log exception
                    throw ex;
                } 
            }
            return operationResult;
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            throw new NotImplementedException();
        }

        public IMetadataProvider GetMetadataProvider()
        {
            return metadataProvider;
        }

        public string PreConnect(IDictionary<string, string> properties)
        {
            return BuildUiFormDefinition();
        }

        protected void InitializeConnector()
        {

            dataAccessProvider = new DataAccessProvider();
            metadataProvider = new Metadata.MetadataProvider(); 
        }

        #region Private Members
        /// <summary>
        /// Construct an XML serialized form that will define the input properties that will be displayed in the UI and 
        /// used to pass back into the connection using the connect method the connection method
        /// </summary>
        /// <returns></returns>
        /// 

        private string BuildUiFormDefinition()
        {
            var formDefinition = new FormDefinition
                                     {
                                         //Add your company name here, this is a required field 
                                         CompanyName = "Scribe Software © 1996-2014. All rights reserved.",
                                         //declare the key used for transfering of sensative data
                                         //this is a required property 
                                       
                                         CryptoKey = cryptoKey,
                                     };

            formDefinition.Add(new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        PropertyName = RootFolderPropertyName,
                                        Label = "Root Folder Path",
                                        Order = 1,
                                        IsRequired = true
                                    });

            return formDefinition.Serialize();
        }

        private bool ValidateConnectionParameters(IDictionary<string,string> connectionParameters)
        {
            string KeyExists = string.Empty;
            bool returnVal = false;

            returnVal = connectionParameters.TryGetValue(RootFolderPropertyName, out KeyExists) ?
                                                       ((System.IO.Directory.Exists(KeyExists)) ? true : false) : false;

            return returnVal;

        }
        #endregion
    }
}
