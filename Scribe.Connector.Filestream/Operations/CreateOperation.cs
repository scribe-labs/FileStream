﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scribe.Connector.FileStream.DataAccess;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Connector.FileStream.Metadata;

namespace Scribe.Connector.FileStream.Operations
{
    internal class CreateOperation : IOperation
    {
        private readonly IDataAccess dataAccess;
        private readonly string operationType;
        private byte[] fileData;

        public CreateOperation(IDataAccess dataAccess, string operation)
        {
            this.dataAccess = dataAccess;
            this.operationType = operation;
        }

        public OperationResult Execute(OperationInput input)
        {
            var operationResult = new OperationResult();

            //Call data access.execute
            string fileName = OperationUtilities.ParseInput(input);

            switch (operationType)
            {
                case "ReadFile":
                    operationResult = this.dataAccess.ExecuteInsert(fileName);
                    break;
                case "WriteFile":
                    fileData = OperationUtilities.ParseDataInput(input);
                    operationResult = this.dataAccess.ExecuteInsert(fileName,fileData);
                    break;
                default:
                    throw new InvalidOperationException();
            }


            //Return result
            return operationResult;
        }

        public void Dispose()
        {
            this.dataAccess.Dispose();
        }
       
    }
}
