﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Scribe.Core.ConnectorApi.Actions;

namespace Scribe.Connector.FileStream.Operations
{
    //TODO implement a single version of this
    internal interface IOperationProvider
    {
        OperationResult Execute(OperationInput operationInput);
    }
}
