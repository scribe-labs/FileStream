﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Connector.FileStream.DataAccess;
using Scribe.Connector.FileStream.Operations;

namespace Scribe.Connector.FileStream.Operations
{

    internal class OperationProvider
    {

        public IOperation GetOperation(OperationInput operationInput, IDataAccessProvider dataAccessProvider)
        {
            string filePath = OperationUtilities.ParseInput(operationInput);
            var dataAccess = dataAccessProvider.GetDataAccess(filePath);

            IOperation operation; 

            string operationType = operationInput.Name;

            switch (operationType)
            {
                case "ReadFile":
                case "WriteFile":
                    operation = new CreateOperation(dataAccess, operationType);
                    break;
                default:
                    throw new InvalidOperationException();
            }

            return operation;
        }

        

    }


}
