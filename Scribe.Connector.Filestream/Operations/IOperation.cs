﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Scribe.Core.ConnectorApi.Actions;

namespace Scribe.Connector.FileStream.Operations
{
    internal interface IOperation: IDisposable
    {
        OperationResult Execute(OperationInput input);
    }
}
