﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scribe.Connector.FileStream.Metadata;
using Scribe.Core.ConnectorApi.Actions;

namespace Scribe.Connector.FileStream.Operations
{
    public static class OperationUtilities
    {
        /// <summary>
        /// Pulls the file path from the operation input and returns it as a string
        /// </summary>
        /// <param name="operationInput"></param>
        /// <returns></returns>
        public static string ParseInput(OperationInput operationInput)
        {
            string path = string.Empty;
            // Parse the input and extract the path string
            if (operationInput.Input.Count() > 0 && operationInput.Input[0].Properties.Count > 0)
            {
                var properties = operationInput.Input[0].Properties.FirstOrDefault(kvp => 
                    kvp.Key == MetadataProvider.FILENAME);


                if (!properties.Equals(default(KeyValuePair<string, object>)))
                {
                    path = properties.Value.ToString(); 
                }
            }

            return path;

        }
        public static byte[] ParseDataInput(OperationInput operationInput)
        {
            byte[] rawdata = new byte[1];
            // Parse the input and extract the byte data
            if (operationInput.Input.Count() > 0 && operationInput.Input[0].Properties.Count > 0)
            {
                var properties = operationInput.Input[0].Properties.FirstOrDefault(kvp =>
                    kvp.Key == MetadataProvider.FILEDATA);


                if (!properties.Equals(default(KeyValuePair<string, object>)))
                {
                    //rawdata = System.Convert.FromBase64String(properties.Value.ToString());
                    rawdata = (byte[])(properties.Value); 
                }
            }

            return rawdata;

        }
    }
}
