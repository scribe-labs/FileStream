
--Create master file table
----------------------------------------------------
CREATE TABLE MasterFileList (
PFLID INT PRIMARY KEY IDENTITY (1,1),
FileNamePath varchar(1000),
FileDateTime datetime ,
FileDrive varchar(50) ,
ParsedFilePath varchar(255) ,
FileName1 varchar(255) ,
FileExt varchar(50) ,
FileSize varchar(50),
UpdateFile bit,
MarkForDelete bit,
)
go

--Create Update view
Create view UpdateFiles as 
Select * from MasterFileList
Where UpdateFile = 'True'
go

--Create Delete view
Create view DeleteFiles as 
Select * from MasterFileList
Where MarkForDelete = 'True'
go

--Create SQL split function
----------------------------------------
CREATE FUNCTION fnSplit(
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    RETURN
END
Go


--Enable files system commands in SQL Server
----------------------------------
EXEC sp_configure 'show advanced options', 1
go
reconfigure
go
EXEC master.dbo.sp_configure 'xp_cmdshell', 1
go
RECONFIGURE
go

--Create Scanning Procedure
Create Procedure ScanForFiles  @SourceFilePath varchar(200)
AS
Begin
 
SET NOCOUNT ON

-- 1 - Variable declarations
DECLARE @CMD1 varchar(5000) 
DECLARE @CMD2 varchar(5000)


DECLARE @FileNamePath varchar(1000)
DECLARE @FileDateTime datetime
DECLARE @FileDrive varchar(50)
DECLARE @ParsedFilePath varchar(255)
DECLARE @FileName1 varchar(255)
DECLARE @FileExt varchar(50)
DECLARE @FileSize varchar(50)	

DECLARE @col1 VARCHAR(max)
DECLARE @getcol CURSOR
DECLARE @getfields CURSOR



-- 2 - Create the #OriginalFileList temporary table to support the un-cleansed file list
CREATE TABLE #OriginalFileList (
Col1 varchar(max) NULL
)

-- 3 - Create the #ParsedFileList temporary table to suppor the cleansed file list
CREATE TABLE #ParsedFileList (
PFLID INT PRIMARY KEY IDENTITY (1,1) ,
FileNamePath varchar(1000),
FileDateTime datetime ,
FileDrive varchar(50) ,
ParsedFilePath varchar(255) ,
FileName1 varchar(255) ,
FileExt varchar(50),
FileSize varchar(50) ,
UpdateFile bit,
MarkForDelete bit,
)


-- 4 - Initialize the variables
SELECT @CMD1 = ''
SELECT @CMD2 = '' 

-- 5 - Build the string to capture the file names in the restore location
SELECT @CMD1 = 'master.dbo.xp_cmdshell ''for /r ' + @SourceFilePath + ' %i in (*) do @echo %~fnxi, %~ti, %~di, %~pi, %~nxi, %~xi, %~zi'''



-- 6 - Build the string to populate the #OriginalFileList temporary table
SELECT @CMD2 = 'INSERT INTO #OriginalFileList(Col1)' + char(13) +
'EXEC ' + @CMD1

-- 7 - Execute the string to populate the #OriginalFileList table
EXEC (@CMD2)


-- 8 - Populate the #ParsedFileList table with File Data
SET @getcol = CURSOR FOR
Select COL1 from #OriginalFileList

OPEN @getcol
FETCH NEXT
FROM @getcol INTO @col1
WHILE @@FETCH_STATUS = 0
BEGIN
   
	   --Parse comma delimited text into fields
		SET @getfields = CURSOR for
		SELECT val FROM dbo.fnSplit(@col1, ',')

		OPEN @getfields
		FETCH NEXT
		FROM @getfields INTO @FileNamePath
		FETCH NEXT
		FROM @getfields INTO @FileDateTime
		FETCH NEXT
		FROM @getfields INTO @FileDrive
		FETCH NEXT
		FROM @getfields INTO @ParsedFilePath
		FETCH NEXT
		FROM @getfields INTO @FileName1
		FETCH NEXT
		FROM @getfields INTO @FileExt
		FETCH NEXT
		FROM @getfields INTO @FileSize

	--get next file info
	FETCH NEXT
    FROM @getcol INTO @col1
	
	If @@FETCH_STATUS = 0
	Begin
		Insert into #ParsedFileList 
		(FileNamePath,FileDateTime,FileDrive,ParsedFilePath,FileName1,FileExt,FileSize,UpdateFile,MarkForDelete) 
		values (@FileNamePath,@FileDateTime,@FileDrive,@ParsedFilePath,@FileName1,@FileExt,@FileSize,'False','False')
	end	
	
END

CLOSE @getfields
DEALLOCATE @getfields

CLOSE @getcol
DEALLOCATE @getcol



--9  -Analyze file list for new/updated items 


CREATE TABLE #UpdateFileList (
PFLID INT PRIMARY KEY IDENTITY (1,1),
FileNamePath varchar(1000),
FileDateTime datetime ,
FileDrive varchar(50) ,
ParsedFilePath varchar(255) ,
FileName1 varchar(255) ,
FileExt varchar(50) ,
FileSize varchar(50),
UpdateFile bit,
MarkForDelete bit,
)

--Insert new files into the updatefilelist table
Insert into #UpdateFileList
Select FileNamePath,FileDateTime,FileDrive,ParsedFilePath,FileName1,FileExt,FileSize,UpdateFile, 'False'
from #ParsedFileList 
Where FileNamePath not in (Select FileNamePath from dbo.MasterFileList)

--Insert files with new timestamps into updatefilelist
Insert into #UpdateFileList
Select a.FileNamePath,a.FileDateTime,a.FileDrive,a.ParsedFilePath,a.FileName1,a.FileExt,a.FileSize,a.UpdateFile, 'False'
From #ParsedFileList a
Where 
a.FileNamePath in  
(Select b.FileNamePath from dbo.MasterFileList b
Where b.FileNamePath = a.FileNamePath and b.FileDateTime <> a.FileDateTime
)


--Insert new/updated files into the MasterFileList table
Update #UpdateFileList set UpdateFile = 'True'

--Delete old data from MasterFileList table
Delete from dbo.MasterFileList 
Where FileNamePath in (Select b.FileNamePath from #UpdateFileList b)

Insert into dbo.MasterFileList
Select FileNamePath,FileDateTime,FileDrive,ParsedFilePath,FileName1,FileExt,FileSize,UpdateFile, 'False'
from #UpdateFileList 

--Scan for Deleted
Update dbo.MasterFileList 
Set MarkForDelete = 'False'

Update dbo.MasterFileList 
Set MarkForDelete = 'True'
Where FileNamePath not in (Select b.FileNamePath from #ParsedFileList b)


-- 10 - Drop the temporary tables
DROP TABLE #OriginalFileList
DROP TABLE #ParsedFileList
DROP TABLE #UpdateFileList


SET NOCOUNT OFF


end
