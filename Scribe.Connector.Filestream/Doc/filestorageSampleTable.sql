CREATE TABLE [dbo].[FileStorage](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](max) NULL,
	[FilePath] [varchar](max) NULL,
	[FileData] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO