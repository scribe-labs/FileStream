﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;

namespace Scribe.Connector.FileStream.Metadata
{
    public class MetadataProvider  : IMetadataProvider
    {
        public const string FILE = "file";
        public const string FILENAME = "filepath";
        public const string FILEDATA = "filedata";


        public void ResetMetadata()
        {
            
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            //var actionDef = new ActionDefinition()
            //{
            //    Description = "Read",
            //    FullName = "Read",
            //    Name = "Read",
            //    SupportsInput = true,
            //    KnownActionType = KnownActions.Create,
            //    SupportsBulk = false,

            //};

            //yield return actionDef;


            return new List<IActionDefinition>
                   {
                       new ActionDefinition
                       {
                            Description = "ReadFile",
                            FullName = "ReadFile",
                            Name = "ReadFile",
                            SupportsInput = true,
                            KnownActionType = KnownActions.Create,
                            SupportsBulk = false,
                       },
                       new ActionDefinition
                       {
                            Description = "WriteFile",
                            FullName = "WriteFile",
                            Name = "WriteFile",
                            SupportsInput = true,
                            KnownActionType = KnownActions.Create,
                            SupportsBulk = false,
                       },
                   };


        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            var objectDefinition = new ObjectDefinition()
            {
                Name = FILE,
                FullName = FILE,
                Description = FILE,
                Hidden = false,
                PropertyDefinitions = CreatePropertyDefinitions(),
                RelationshipDefinitions = new List<IRelationshipDefinition>(),
                SupportedActionFullNames = new List<string>(){"ReadFile","WriteFile"}

            };

            return objectDefinition;
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            yield return RetrieveObjectDefinition(string.Empty, shouldGetProperties, shouldGetRelations);
        }

        private List<IPropertyDefinition> CreatePropertyDefinitions()
        {
            var propertyDefinitions = new List<IPropertyDefinition>();
            propertyDefinitions.Add(new PropertyDefinition()
            {
                Name = FILENAME,
                FullName = FILENAME,
                Description = "Full name of file",
                //Size = 
                IsPrimaryKey = false,
                UsedInQuerySelect = false,
                //query is all read false
                //used in action inputs are also false
                UsedInActionInput = true,
                RequiredInActionInput = true,
                PropertyType = typeof(System.String).ToString()
            });
            propertyDefinitions.Add(new PropertyDefinition()
            {
                Name = FILEDATA,
                FullName = FILEDATA,
                Description = "File Data",
                IsPrimaryKey = false,
                UsedInQuerySelect = false,
                UsedInActionInput = true,
                RequiredInActionInput = false,
                PropertyType = typeof(System.Byte[]).ToString(),

            }    
            );
            return propertyDefinitions;
        }

        public void Dispose()
        {
            
        }

        #region method def
        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {

            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
